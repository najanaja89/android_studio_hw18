package com.example.hw18;

import android.os.Bundle;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.LinkedList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {

    RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        recyclerView = findViewById(R.id.recyclerView);

        LinkedList<Item> items = new LinkedList<Item>();
        //items.add(new Item("1","title", "body"));

        ListElements listElements = new ListElements(items);


        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://jsonplaceholder.typicode.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        MessageApi api = retrofit.create(MessageApi.class);
        Call<List<User>> users = api.getUsers();
        users.enqueue(new retrofit2.Callback<List<User>>() {

            @Override
            public void onResponse(Call<List<User>> call, Response<List<User>> response) {
                int size = response.body().size();
                for (int i = 0; i < size; i++) {
                    String id = response.body().get(i).id;
                    String userId = response.body().get(i).userId;
                    String title = response.body().get(i).title;
                    String body = response.body().get(i).body;

                    Log.d("Hello", id + " " + userId + " " + title + " " + body);
                    listElements.addItem(new Item(userId, title, body));
                }

                MyAdapter myAdapter = new MyAdapter(listElements.getListItems());
                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(MainActivity.this);
                recyclerView.setLayoutManager(linearLayoutManager);
                recyclerView.setAdapter(myAdapter);
            }

            @Override
            public void onFailure(Call<List<User>> call, Throwable t) {
                t.printStackTrace();
            }
        });


    }

}