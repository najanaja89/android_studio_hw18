package com.example.hw18;

public class User {
    String id;
    String userId;
    String title;
    String body;

    public User(String id, String userId, String title, String body) {
        this.id = id;
        this.userId = userId;
        this.title = title;
        this.body = body;
    }
}
