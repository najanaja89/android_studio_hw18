package com.example.hw18;

import java.io.Serializable;

public class Item  implements Serializable {

    String userId;
    String title;
    String body;

    public Item(String userId, String title, String body) {

        this.userId = userId;
        this.title = title;
        this.body = body;
    }
}
