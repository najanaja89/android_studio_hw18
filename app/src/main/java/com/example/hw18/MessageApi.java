package com.example.hw18;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface MessageApi {
    @GET("posts")
    Call<List<User>> getUsers();
}
