package com.example.hw18;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.LinkedList;
import java.util.List;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder>{
    private LinkedList<Item> data;
    private MyOnClickListener onClickListener;
    private MyOnTouchListener onTouchListener;
    private MyOnLongClickListener onLongClickListener;

    public MyAdapter(LinkedList<Item> data){
        this.data = data;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_item, parent, false);

        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        holder.userId.setText(data.get(position).userId);
        holder.body.setText(data.get(position).body);
        holder.title.setText(data.get(position).title);



        if(onClickListener !=null){
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onClickListener.onClick(data.get(position));
                }
            });
        }

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public interface MyOnClickListener {
        void onClick(Item item);
    }

    public interface MyOnTouchListener {
        void onTouch(Item item);
    }

    public interface MyOnLongClickListener {
        void onLongClick(Item item);
    }

    void setListener(MyOnClickListener onClickListener){
        this.onClickListener = onClickListener;
    }
    void setOnTouchListener(MyOnTouchListener onTouchListener) {this.onTouchListener = onTouchListener;}
    void setOnLongClickListener(MyOnLongClickListener onLongClickListener) {this.onLongClickListener = onLongClickListener;}

    static class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView body;
        public TextView userId;
        public TextView title;
        public Spinner spinner;

        public Context myContext;

        public MyViewHolder(@NonNull View v) {
            super(v);

            myContext = v.getContext();
            userId = v.findViewById(R.id.userIdTextView);
            title = v.findViewById(R.id.titleTextView);
            body = v.findViewById(R.id.bodyTextView);

        }
    }
}
